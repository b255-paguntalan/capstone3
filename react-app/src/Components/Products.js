import { Card } from "react-bootstrap";

export default function ProductCard({productProp}) {

    const {productName, description, price, _id, img} = productProp

    return (
        
        <Card id="productCard" className="m-3 d-flex flex-column justify-content-evenly" onClick={() => window.location.href=`http://localhost:3000/products/${_id}`}>
            <Card.Body>

                <div >
                    <img id="productImg" src={img} />
                </div>

                <div id="productBody" className="text-center">
                    <Card.Title id="productName" className="mt-4 "><b>{productName}</b></Card.Title>
                    
                    <Card.Text id="productPrice" className="mb-2 "><b>₱ {price}</b></Card.Text>

                    <Card.Text id="productDesc" className="">{description}</Card.Text>
                </div>
                
            </Card.Body>
        </Card>
        
    )
}