import { useEffect, useState } from "react"
import { Card, Container, Row } from "react-bootstrap"

export default function Cart() {

    const [email, setEmail] = useState("")
    const [orders, setOrders] = useState([])

    useEffect(() => {
        fetch(`http://localhost:4000/users/details`, {
            method:'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "id": localStorage.getItem("id")
            })
        })
        .then(res => res.json())
        .then(result => {
            if(result !== null) {
                console.log(result)

                setEmail(result.email)
                setOrders(result.orderedProduct)
            }
        })
    }, [])

    return (
        <Container fluid className="min-vh-100 pt-5 my-5">

            <h2 className="text-center">Welcome to your cart <b id="email">{email}</b>!</h2>

            <Row className="d-flex justify-content-center">

            {orders.map(order => 
            (

            <Card id="productCard2" className="m-3 d-flex flex-column justify-content-evenly">
                <div key={order._id}>
                
                    {order.products.map(product => (
                    <div key={product._id}>

                        <img id="productImg" src={product.img}></img>

                        <div id="productBody" className="text-center py-5">
                            <Card.Title id="cartname"><b>{product.productName}</b></Card.Title>
                            <Card.Text className="pt-4"><b>Quantity: {product.quantity}</b></Card.Text>
                            <Card.Text><b>Total: ₱ {order.totalAmount}</b></Card.Text>
                        </div>
                        
                    </div>
                    ))}

                </div>
            </Card>

            )
            )}

            </Row>

        </Container>
    )
}
