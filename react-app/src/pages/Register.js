import {Form, Container, Button} from "react-bootstrap";
import { useContext, useEffect, useState } from "react";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { useNavigate, Navigate } from 'react-router-dom';


export default function Register() {

    const [email, setEmail] = useState("")
    const [password1, setPassword1] = useState("")
    const [password2, setPassword2] = useState("")
    const [isActive, setIsActive] = useState("")

    const {user} = useContext(UserContext)

    function registerUser(e) {
        e.preventDefault();

        fetch(`http://localhost:4000/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "email" : email,
                "password" : password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
    
            if(data === true) {
    
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Redirecting to login page"
                }).then(() => {
                    window.location.href = "/login";
                }); 
            }
            else {
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide a different email."
                })
            }
        })

        setEmail("");
        setPassword1("");
        setPassword2("");
    }

    useEffect( () => {
        if( (email !== "" && password1!=="" && password2!=="") && (password1 === password2) ) {
            setIsActive(true)
        }
    }, [email, password1, password2])

    return (

        (localStorage.getItem("id") !== null) ?
            <Navigate to="/"/>
        :

        <Container fluid className="min-vh-100 d-flex flex-column justify-content-center align-items-center pt-5">

            <Form id="rform" onSubmit={(e) => registerUser(e)}>

                <Form.Group className="text-center mt-3">
                    <Form.Label id="label"><b>Register</b></Form.Label>
                </Form.Group>

                <Form.Group className="m-3">
                    <Form.Label className="labels"><b>Email</b></Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        required
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="m-3">
                    <Form.Label className="labels"><b>Password</b></Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter password"
                        required
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="m-3">
                    <Form.Label className="labels"><b>Confirm Password</b></Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter password again"
                        required
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="text-center">
                    {isActive ? 

                        <Button variant="primary" type="submit" id="submitBtn">
                            Submit
                        </Button>
                        :
                        <Button variant="primary" type="submit" id="submitBtn" disabled>
                            Submit
                        </Button>
                    }
                </Form.Group>

                <Form.Group className="text-center my-4">
                    <p>Already have an account? <a href="/login">Login</a></p>
                </Form.Group>

            </Form>

        </Container>
    )
}