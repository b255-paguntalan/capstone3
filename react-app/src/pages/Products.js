import { useEffect, useState } from "react";
import ProductCard from "../Components/Products";
import { Container, Row } from "react-bootstrap";

export default function Products() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch("http://localhost:4000/products/allProducts")
            .then(res => res.json())
            .then(data => {
                const activeProducts = data.filter(product => product.isActive);
                const mappedProducts = activeProducts.map(product => (
                    <ProductCard key={product._id} productProp={product}></ProductCard>
                ));
                setProducts(mappedProducts);
            });
    }, []);

    return (
        <Container fluid className="min-vh-100 pt-5 mt-5 mb-5">
            <Row className="d-flex justify-content-center">
                {products}
            </Row>
        </Container>     
    );
}
