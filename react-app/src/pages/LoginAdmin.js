import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function LoginAdmin() {

    const { user, setUser} = useContext(UserContext); 


	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
  
    const [isActive, setIsActive] = useState(true);


    function authenticate(e) {

    e.preventDefault();

    fetch(`http://localhost:4000/users/loginAdmin`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        if(data === false){

            Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Please try again"
            });


        } else {

            localStorage.setItem('token', data.access);
            localStorage.setItem('id', data.result[0]._id);
            localStorage.setItem('isAdmin', "true");
            localStorage.setItem('archive', "false")

            setUser({
                isAdmin: true
            })

            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome Admin!"
            });
        };
    })

    setEmail('');
    setPassword('');

	}

    useEffect(() => {

    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

	}, [email, password]);

	

    return (
   
        (localStorage.getItem("id") !== null) ?
        <Navigate to={"/"}/>
        :

        <Container fluid className="min-vh-100 d-flex flex-column justify-content-center align-items-center">
            <Form id="aform" onSubmit={(e) => authenticate(e)}>
                <Form.Group id="label" className="text-center mt-3">
                    <Form.Label><b>Login Admin</b></Form.Label>
                </Form.Group>

                <Form.Group className="m-3">
                    <Form.Label className="labels"><b>Email</b></Form.Label>
                    <Form.Control
                    type="email"
                    placeholder="Enter Email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    ></Form.Control>
                </Form.Group>

                <Form.Group className="m-3">
                    <Form.Label className="labels"><b>Password</b></Form.Label>
                    <Form.Control
                    type="password"
                    placeholder="Enter Password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    ></Form.Control>
                </Form.Group>

                <Form.Group className="text-center">
                    {isActive ? 
                        <Button type="submit" id="submitbtn">Submit</Button>
                        :
                        <Button type="submit" id="submitbtn" disabled>Submit</Button>
                    }
                </Form.Group>

                <Form.Group className="text-center my-4">
                    <p>Don't have an account? <a href="/register">Register</a></p>
                </Form.Group>

            </Form>
        </Container>


    )
}

