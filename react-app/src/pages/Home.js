import { Button, Container } from "react-bootstrap";
import bgVid from '../images/bg.mp4';
import Swal from 'sweetalert2';

export default function Home() {

    const checkAdmin = () => {
        if (localStorage.getItem('token') === null) {
            window.location.href="/products";
        }
        else {
            window.location.href="/productsAdmin";
        }
    }

    return (
        <div className="min-vh-100">
            <video src={bgVid} autoPlay muted loop />
            <div id="hContent" className="d-flex flex-column justify-content-center align-items-center text-center">
                <div id="welcome">Welcome to eStore!</div>
                
                {
                    (localStorage.getItem("id") !== null) ?
                    <div id="start" className="pt-1">
                        Start browsing <Button id="toProducts" onClick={checkAdmin} className="pb-2 px-3 rounded-pill"><b>Products</b></Button>
                    </div>
                    :
                    <div id="start" className="pt-1">
                        Login to start browsing <Button id="toProducts" onClick={() => window.location.href="/login"} className="pb-2 px-3 rounded-pill"><b>Login</b></Button>
                    </div>
                }

            </div> 
        </div>
    )
}
