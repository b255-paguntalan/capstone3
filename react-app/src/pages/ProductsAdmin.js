import { useEffect, useState } from "react";
import ProductCard from "../Components/Products";
import { Container, Row } from "react-bootstrap";

export default function ProductsAdmin() {
    const [productsAdmin, setProductsAdmin] = useState([])

    useEffect( () => {
        fetch("http://localhost:4000/products/allProducts")
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setProductsAdmin(data.map(productAdmin => {
                return (
                    <ProductCard key={productAdmin._id} productProp={productAdmin}></ProductCard>
                )
            }))
        })

    }, [])

    return (
   
    <Container fluid className="min-vh-100 pt-4 mt-5 mb-5">

        <div className="text-center pb-3">
            <h1><b>Welcome admin!</b></h1>
            <button id="add" className="px-3 py-2 rounded-pill" onClick={() => window.location.href="/add"}>Add Product</button>
        </div>
        
        <Row className="d-flex justify-content-center">
            {productsAdmin}
        </Row>
        
    </Container>     

    )

}