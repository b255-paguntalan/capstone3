import { useEffect, useState } from "react";
import { Card, Container, Row, Col, Form, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function FullProduct() {

    const [img, setImg] = useState("")
    const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const { productId } = useParams();
    const [active, setActive] = useState("")

    const [productImage, setProductImage] = useState("")
    const [productName, setProductName] = useState("")
    const [productDescription, setProductDescription] = useState("")
    const [productPrice, setProductPrice] = useState("")
    const [num, setNum] = useState(1)

    const [clicked, setClicked] = useState(false)

    const token = localStorage.getItem("token")

	useEffect(() => {
		console.log(productId);

		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {

            setImg(data.img)
			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
            setActive(data.isActive);

            setProductImage(data.img);
            setProductName(data.productName);
            setProductDescription(data.description);
			setProductPrice(data.price);

            console.log(active)
		})

	}, [productId]);

    const update = (u) => {
        u.preventDefault()

        fetch(`http://localhost:4000/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ token }`
            },
            body: JSON.stringify({
                "img" : productImage,
                "productName" : productName,
                "description" : productDescription,
                "price" : productPrice
            })
        })
        .then(res => res.json())
        .then(result => {
            console.log(result)

            if(result === true) {
                Swal.fire({
                    title: "Update Successful",
                    icon: "success",
                }).then(() => {
                    window.location.reload();
                }); 
            }
            else {
                Swal.fire({
                    title: "Oops! Something went wrong.",
                    icon: "error",
                    text: "Please try again."
                })
            }
        })

        setProductImage("")
        setProductName("")
        setProductDescription("")
        setProductPrice("")
    }

    const archive = () => {
        fetch(`http://localhost:4000/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(archive => {
            console.log(archive)
            if(archive === true) {
                Swal.fire({
                    title: "Product successfully archived!",
                    icon: "success"
                }).then(() => {
                    window.location.reload();
                }); 

            }
            else {
                Swal.fire({
                    title: "Archive unsuccessful!",
                    icon: "error"
                })
            }
        })
    }

    const activate = () => {
        fetch(`http://localhost:4000/products/${productId}/activate`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(activate => {
            console.log(activate)
            if(activate === true) {
                Swal.fire({
                    title: "Product successfully activated!",
                    icon: "success"
                }).then(() => {
                    window.location.reload();
                }); 

            }
            else {
                Swal.fire({
                    title: "Activate unsuccessful!",
                    icon: "error"
                })
            }
        })
    }

    const deleteP = () => {
        fetch(`http://localhost:4000/products/${productId}/delete`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(resultd => {
            console.log(resultd)
            if(resultd === true) {
                Swal.fire({
                    title: "Product successfully deleted!",
                    icon: "success"
                })
                .then(() => {
                    window.location.href="/productsAdmin"
                })
                
            }
            else {
                Swal.fire({
                    title: "Delete unsuccessful!",
                    icon: "error"
                })
            }
        })
    }

    const btn = () => {
        setClicked(true)
    }

    const buy = (b) => {
        b.preventDefault();

        fetch(`http://localhost:4000/users/buy`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "userId" : localStorage.getItem("id"),
                "productId": `${productId}`,
                "quantity": num
            })
        })
        .then(res => res.json())
        .then(buy => {
            if(buy === true) {
                Swal.fire({
                    title: "Product added to cart!",
                    icon: "success"
                }).then(() => {
                    window.location.href="/cart";
                }); 

            }
            else {
                Swal.fire({
                    title: "Oops.. please try again!",
                    icon: "error"
                })
            }
        })
    }

	return(

        localStorage.getItem("isAdmin") === "false" ? 

		<Container fluid className=" min-vh-100 d-flex flex-column justify-content-center align-items-center py-5">
			
            <Card id="fcard" className="col-10 col-md-8 mt-5">
                <Card.Body className="text-center">
                    
                    <Row className="d-flex justify-content-evenly">
                        <div  id="fullProductImgCon" className="my-auto">
                            <img id="fullProductImg" src={img}></img>
                        </div>

                        <div  id="fProductCon" className="mt-3">
                            <Card.Title id="fProductTitle"><b>{name}</b></Card.Title>
                            <Card.Text id="fProductPrice" className="mt-3"><b>₱ {price}</b></Card.Text>
                            <Card.Text id="fProductDesc">{description}</Card.Text>

                            {
                                clicked ?

                                <div className="d-flex flex-column">
                                    <div><button id="con" className="px-4 py-2" onClick={buy}><b>Confirm</b></button></div>
                                    <div><input id="input" className="my-3 px-1" type="number" placeholder="input number" value={num} onChange={b => setNum(b.target.value)}></input></div>
                                </div>
                            :
                                <div>
                                    <button id="buy" className="px-4 py-2" onClick={btn}><b>Add to Cart</b></button>
                                </div>
                            }
                        </div>
                    </Row>
                    
                </Card.Body>
            </Card>
            
		</Container>
        
        :

        <Container fluid className=" mt-5 min-vh-100 d-flex flex-column justify-content-center align-items-center">

            <Card id="fcard" className="col-10 col-md-8 mt-5 mb-5">
                <Card.Body>
                    
                    <Row className="d-flex justify-content-evenly text-center">
                        <div  id="fullProductImgCon" className="my-auto">
                            <img id="fullProductImg" src={img}></img>
                        </div>

                        <div  id="fProductCon" className="mt-3">
                            <Card.Title id="fProductTitle"><b>{name}</b></Card.Title>
                            <Card.Text id="fProductPrice" className="mt-3"><b>₱ {price}</b></Card.Text>
                            <Card.Text id="fProductDesc">{description}</Card.Text>
                            <div className="d-flex my-3 justify-content-center">
                                <div>
                                    <button id="del" className="deldea rounded-pill mx-2" onClick={deleteP}><b>Delete</b></button>
                                </div>

                                {
                                    active === true ?
                                <div>
                                    <button id="dea" className="deldea rounded-pill mx-2" onClick={archive}><b>Archive</b></button>
                                </div>
                                :
                                <div>
                                    <button id="dea" className="deldea rounded-pill mx-2" onClick={activate}><b>Activate</b></button>
                                </div>
                                }

                            </div>
                        </div>
                    </Row>

                    <Form id="uform" className="mt-2 p-3" onSubmit={(i) => update(i)}>

                        <Form.Group className="my-3">
                            <Form.Label>
                                <b id="alabel">Edit Product</b>
                            </Form.Label>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>
                                <b id="flabel">Product Image</b>
                            </Form.Label>

                            <Form.Control
                                type="textarea"
                                value={productImage}
                                onChange={u => setProductImage(u.target.value)}
                            ></Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>
                                <b id="flabel">Product Name</b>
                            </Form.Label>

                            <Form.Control
                                type="textarea"
                                value={productName}
                                onChange={u => setProductName(u.target.value)}
                            ></Form.Control>
                        </Form.Group>
                            
                        <Form.Group>
                            <Form.Label>
                                <b id="flabel">Product Description</b>
                            </Form.Label>

                            <Form.Control
                                type="textarea"
                                value={productDescription}
                                onChange={u => setProductDescription(u.target.value)}
                            ></Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>
                                <b id="flabel">Product Price</b>
                            </Form.Label>

                            <Form.Control
                                type="number"
                                value={productPrice}
                                onChange={u => setProductPrice(u.target.value)}
                            ></Form.Control>
                        </Form.Group>

                        <Form.Group className="text-center mt-3">

                            <Button type="submit" id="submitbtn">Submit</Button>

                        </Form.Group> 

                    </Form>
                    
                </Card.Body>
            </Card>
            
		</Container>

	)

}