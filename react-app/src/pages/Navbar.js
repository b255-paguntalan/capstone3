import { Navbar, Container, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import React, { useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar() {

  const { user } = useContext(UserContext)

  return (
    <Navbar collapseOnSelect id="navbar" expand="md" className="fixed-top navbar  bg-light">
      <Navbar.Brand className="ms-2 ms-md-5" as={Link} to="/">eStore |</Navbar.Brand>

      <Navbar.Toggle className="me-2" aria-controls="basic-navbar-nav" />

      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto me-5">

          {
            (localStorage.getItem("id") !== null) ?

              <React.Fragment>
                <Nav.Link eventKey="1" as={NavLink} to="/">Home</Nav.Link>

                {

                 	localStorage.getItem("isAdmin") === "true" ? 
                  <Nav.Link eventKey="2" as={NavLink} to="/productsAdmin" exact>Products</Nav.Link>

                 	: 

                	<React.Fragment>
                    <Nav.Link eventKey="2" as={NavLink} to="/products" exact>Products</Nav.Link>
                    <Nav.Link eventKey="6" as={NavLink} to="/cart">Cart</Nav.Link>
                  </React.Fragment>
                  
                }
              </React.Fragment>
              :
              <Nav.Link eventKey="1" as={NavLink} to="/">Home</Nav.Link>

          }

          {(localStorage.getItem("id") !== null) ?
            <Nav.Link eventKey="3" as={NavLink} to="/logout">Logout</Nav.Link>
            :
            <React.Fragment>
              <Nav.Link eventKey="4" as={NavLink} to="/login">Login</Nav.Link>
              <Nav.Link eventKey="5" as={NavLink} to="/register">Register</Nav.Link>
            </React.Fragment>
          }


        </Nav>
      </Navbar.Collapse>
    </Navbar>

  )
}
