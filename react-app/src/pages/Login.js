import { Container, Form, Button } from "react-bootstrap";
import { useContext, useEffect, useState } from "react";
import Swal from "sweetalert2";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";


export default function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState("");

    const {user, setUser} = useContext(UserContext);

    function loginUser(e) {
        e.preventDefault()

        fetch(`http://localhost:4000/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "email" : email,
                "password" : password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === false) {

                Swal.fire({
                    title: "No email found",
                    icon: "error",
                    text: "Please try again"
                })
            }
            else {

                localStorage.setItem('id', data.result[0]._id);
                localStorage.setItem('isAdmin', "false");

                setUser({
                    isAdmin: false
                })

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to eStore!"
                })
            }
        })

        setEmail("")
        setPassword("")
    }

    useEffect( () => {
        if(email !== "" && password !== "") {
            setIsActive(true)
        }
    }, [email, password])

    return (

        (localStorage.getItem("id") !== null) ?
        <Navigate to={"/"}/>
        :

        <Container fluid className="min-vh-100 d-flex flex-column justify-content-center align-items-center">

            <Form id="lform" onSubmit={(e) => loginUser(e)}>
            
                <Form.Group id="label" className="text-center mt-3">
                    <Form.Label><b>Login</b></Form.Label>
                </Form.Group>

                <Form.Group className="m-3">
                    <Form.Label className="labels"><b>Email</b></Form.Label>
                    <Form.Control
                    type="email"
                    placeholder="Enter Email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    ></Form.Control>
                </Form.Group>

                <Form.Group className="m-3">
                    <Form.Label className="labels"><b>Password</b></Form.Label>
                    <Form.Control
                    type="password"
                    placeholder="Enter Password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    ></Form.Control>
                </Form.Group>

                <Form.Group className="text-center">
                    {isActive ? 
                        <Button type="submit" id="submitbtn">Submit</Button>
                        :
                        <Button type="submit" id="submitbtn" disabled>Submit</Button>
                    }
                </Form.Group>

                <Form.Group className="text-center my-4">
                    <p>Don't have an account? <a href="/register">Register</a></p>
                </Form.Group>

            </Form>
        </Container>
    )
}
