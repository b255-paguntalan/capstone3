import { useEffect, useState } from "react";
import { Button, Card, Container, Form } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Add() {

    const [pimg, setPimg] = useState("")
    const [pname, setPname] = useState("")
    const [pdesc, setPdesc] = useState("")
    const [pprice, setPprice] = useState("")

    const [isFilled, setIsFilled] = useState("")

    const token = localStorage.getItem("token")

    const addProduct = (a) => {
        a.preventDefault()

        fetch(`http://localhost:4000/products`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ token }`
            },
            body: JSON.stringify({
                "img" : pimg,
                "productName" : pname,
                "description" : pdesc,
                "price" : pprice
            })
        })
        .then(res => res.json())
        .then(result => {
            console.log(result)
            if(result === true) {
                localStorage.setItem('archive', "false")
                Swal.fire({
                    title: "Product added successfully!",
                    icon: "success",
                })
                .then( () => {
                    window.location.href="/productsAdmin"
                })
            }
            else {
                Swal.fire({
                    title: "Oops! Something went wrong.",
                    icon: "error",
                    text: "Please try again."
                })
            }
        })

        setPimg("")
        setPname("")
        setPdesc("")
        setPprice("")
    }

    useEffect( () => {
        if(pimg !== "" && pname !== "" && pdesc !== "" && pprice !== "") {
            setIsFilled(true)
        }   
    }, [pimg, pname, pdesc, pprice])

    return (
        <Container fluid className="min-vh-100 pt-5 mt-5 d-flex flex-column align-items-center">

            <Card id="fcard" className="col-10 d-flex flex-column align-items-center m-5">

                <Form id="addform" onSubmit={(a) => addProduct(a)} className="col-11 m-5 p-3">
                    <Form.Group className="my-3">
                        <Form.Label>
                            <b id="alabel">Add product</b>
                        </Form.Label>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>
                            <b id="flabel">Product Image</b>
                        </Form.Label>

                        <Form.Control
                            type="textarea"
                            value={pimg}
                            onChange={a => setPimg(a.target.value)}
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>
                            <b id="flabel">Product Name</b>
                        </Form.Label>

                        <Form.Control
                            type="textarea"
                            value={pname}
                            onChange={a => setPname(a.target.value)}
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>
                            <b id="flabel">Product Description</b>
                        </Form.Label>

                        <Form.Control
                            type="textarea"
                            value={pdesc}
                            onChange={a => setPdesc(a.target.value)}
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>
                            <b id="flabel">Product Price</b>
                        </Form.Label>

                        <Form.Control
                            type="number"
                            value={pprice}
                            onChange={a => setPprice(a.target.value)}
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group className="pt-3 text-center">
                        {
                            isFilled ?
                            <Button type="submit">Submit</Button>
                            :
                            <Button type="submit" disabled>Submit</Button>
                        }
                    </Form.Group>
                </Form>

            </Card>

        </Container>
    )
}