export default function Error() {
    return (
        <div id='error' className='min-vh-100 d-flex justify-content-center align-items-center'>
            <div id="err" >
                <div className="p-5">
                    <h1 id="oops">Oops! Page not found</h1>
                    <p id="goBack">Go back to <a href="/">homepage</a>.</p>
                </div>
            </div>
        </div>
    )
}