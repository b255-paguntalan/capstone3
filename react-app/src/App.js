import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Register from './pages/Register';
import Login from './pages/Login';
import AppNavbar from './pages/Navbar';
import Home from './pages/Home'
import { Container } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { UserProvider } from './UserContext';
import LoginAdmin from './pages/LoginAdmin';
import Logout from './pages/Logout';
import Products from './pages/Products';
import FullProduct from './pages/FullProduct';
import Error from './pages/Error';
import ProductsAdmin from './pages/ProductsAdmin';
import Add from './pages/Add';
import Cart from './pages/Cart';

export default function App() {

  const [user, setUser] = useState({
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  }, [user])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>

      <BrowserRouter>

        
          <AppNavbar></AppNavbar>
          
          <Routes>

            <Route path='/cart' element={<Cart/>} />
            <Route path='/add' element={<Add/>} />
            <Route path='*' element={<Error/>} />
            <Route path='/products/:productId' element={<FullProduct/>} />

            (localStorage.getItem("isAdmin") !== true) ? 
            <Route path='/products' element={<Products/>} />
            :
            <Route path='/productsAdmin' element={<ProductsAdmin/>} />


            <Route path='/logout' element={<Logout/>} />
            <Route path='/loginAdmin' element={<LoginAdmin/>} />
            <Route path='/' element={<Home/>} />
            <Route path='/register' element={<Register/>} />
            <Route path='/login' element={<Login/>} />

          </Routes>
        

      </BrowserRouter>

    </UserProvider>

  )
}


